import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileonboardComponent } from './mobileonboard.component';

describe('MobileonboardComponent', () => {
  let component: MobileonboardComponent;
  let fixture: ComponentFixture<MobileonboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileonboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileonboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
