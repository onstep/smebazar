import { Injectable } from '@angular/core';
import { GlobalService } from '../services/global.service';
import { SignupComponent } from '../components/signup/signup.component';

@Injectable({
  providedIn: 'root'
})
export class PopupsService {

  constructor( public global: GlobalService) {


   }
   opensignup(): void {
    // tslint:disable-next-line: no-use-before-declare
    const dialogRef = this.global.dialog.open(SignupComponent, {
      minWidth: '400px',
      height: '545px',
      hasBackdrop: true,
      data: {selected: 'signup'}

    });

  }
  open(): void {
    // tslint:disable-next-line: no-use-before-declare
    const dialogRef = this.global.dialog.open(SignupComponent, {
      minWidth: '400px',
      height: '545px',
      hasBackdrop: true,
      data: {selected: 'signup'}

    });

  }
}
