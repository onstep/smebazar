import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {
  amount:any;
  rate:any;
  tenure:any;
  value1:any;
  value2:any;
  value3:any;
  value4:any;
  total:any;
  interest:any;
  payment:any;
  constructor(public global: GlobalService) { }

  ngOnInit() {
  }
  result() {
    // this.calculateResults();
  }
  // calculateResults() {
  //   // console.log("calculating");
  //   // Declare UI Variables
  //   const amount = document.getElementById('amount');
  //   const interest = document.getElementById('interest');
  //   const years = document.getElementById('years');
  //   const monthlyPayment = document.getElementById('monthly-payment');
  //   const totalPayment = document.getElementById('total-payment');
  //   const totalInterest = document.getElementById('total-interest');
  //   // Turn amount into decimal and store it into variable
  //   const principal = parseFloat(amount.value);
  //   const calculatedInterest = parseFloat(interest.value) / 100 / 12;
  //   const calculatedPayment = parseFloat(years.value) * 12;
  //   // Compute monthly payments
  //   const x = Math.pow(1 + calculatedInterest, calculatedPayment);
  //   const monthly = (principal * x * calculatedInterest) / (x - 1);
  //   // Check if value is finite
  //   if (isFinite(monthly)) {
  //       monthlyPayment.value = monthly.toFixed(2);
  //       totalPayment.value = (monthly * calculatedPayment).toFixed(2);
  //       totalInterest.value = ((monthly * calculatedPayment) - principal).toFixed(2);
  //   }
  //   else {
  //       this.showError("Please check your numbers");
  //   }
  //   preventDefault();
  // }
   showError(error) {
    // create div
    const errorDiv = document.createElement('div');
    // Get card and heading in order to add new div to DOM. Parent element
    const card = document.querySelector('.card');
    const heading = document.querySelector('.heading');
    // Give div a class name
    errorDiv.className = 'alert alert-danger';
    // Create text and append div
    errorDiv.appendChild(document.createTextNode(error));
    // Insert error above heading. Insert above will take in the parent element which is the card in this case and for the parameters
    // It will take in the element you want to put in and the element you want to put it before, in this case errorDiv and the heading
    card.insertBefore(errorDiv, heading);
    // Clear error after 3 seconds
    setTimeout(this.clearError, 3000);
  }
  // Create clear error
   clearError() {
    document.querySelector('.alert').remove();
  }
  emi()
  {
    console.log(this.amount);
    console.log(this.rate);
    console.log(this.tenure);
    this.value1=Number(this.rate/12/100);
    this.interest=this.amount*this.value1*((1+Number(this.value1))**Number(this.tenure));
    this.payment=((1+Number(this.value1))**this.tenure)-1;
    this.total=(this.interest/this.payment).toFixed();
    this.value2=this.total*this.tenure-this.amount;
this.value3=Number(this.amount)+Number(this.value2);  
}
}
