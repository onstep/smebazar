import { Component, OnInit } from '@angular/core';
import { Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { SignupComponent } from '../../components/signup/signup.component';
import { GlobalService } from '../../services/global.service';
import { PopupsService } from '../../services/popups.service';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {
  clicked='1';
  rocket = {
    path: 'https://assets9.lottiefiles.com/temp/lf20_KTRzeS.json',
    renderer: 'canvas',
    autoplay: true,
    loop: true
};
selected;
otp='no';
constructor( public popups: PopupsService , public global: GlobalService, public dialog: MatDialog) { }

  ngOnInit() {
  //  this.selectit('gg');
  }
  // openDialog(): void {
  //   // tslint:disable-next-line: no-use-before-declare
  //   const dialogRef = this.dialog.open(SignupComponent, {
  //     minWidth: '400px',
  //     height: '545px',
  //     hasBackdrop: true,
  //     data: {selected: 'signup'}

  //   });

  // }


  selectit(type){
    // this.clicked='0';

    // this.otp='getotp';
    
    // return;
    this.selected=type;
    this.clicked='0';
    console.log(this.selected);
  }
  back(){
    this.clicked ='1';
  }
}
