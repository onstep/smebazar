import { Component, OnInit, Input } from '@angular/core';
import { PopupsService } from '../../services/popups.service';
import { GlobalService } from '../../services/global.service';
import { MatSliderChange } from '@angular/material/slider';
import { HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  modal: any = 0;
  flag: any = 0;
  list: any = 0;
  ranging: any = 0;
  rangefrom = 14;
  range: any;
  rangelist: any;
  lis: any;
  listing: any;
  compare: any;
  name: any;
  id: any;
  to: any;
  fil: any;
  newrange: any = {upper: 40 , lower : 10};
  banklist: any;
  filter: any;
  bankname: any = [];
  interstrange: any = {};
  formdata: any = {};
  filterstate : any = 0 ;
  filtertype : any = 0 ;
  userid: any;
  turnover : any;
  loanapply : any;
   @Input('data') data: any;
  loanid: any;
  buttonloading: boolean;
  formInput: any;
  form: any;
  type: any = 'add';
  public isSubmit: boolean;
  loading = false;
  statusloading = [];
  tabledata: any = {};
  editdata: any = {};
  countdata: any = {};
  filelist : any;
  private filesControl :any;

  public addform = new FormGroup({
    //files: this.filesControl
  });
  public uploadedFiles: Array < File > = [];
  public uploadedFiles2: Array < File > = [];
  files: FileList;

  constructor(public popups: PopupsService, public global: GlobalService) { }
str(value){
return JSON.stringify(value);
}
  ngOnInit() {
    this.global.storage.get('userid').then((val) => {
     
     //this.global.toast('funding');
      this.userid=val;
      console.log(this.userid);
      
   
      });

    this.initial();
//this.geting();
  }
  initial() {
    console.log("sme");
  
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );
  
    let temp : any = {};
    temp.id = [];
    // temp.interest = {upper: this.formatLabel(this.newrange.upper), lower: this.formatLabel(this.newrange.lower)};
    temp.interest = this.newrange;
  
   
  
    this.global.http.post(this.global.urls + 'loanlisting.php', temp,  {observe: 'response'})
  
       .subscribe(data => {
        // console.log(data.body);
  
       this.listing = data.body;
  
       this.banklist = this.listing.data;

       this.compare = this.listing.data;
       this.name = this.listing.name;
       console.log(this.name);
       console.log('hii');
  
  
       this.list = 1;
  
  // this.loaded = '1';
       }, error => {
  
       });
  }
  open(value) {
    this.flag = value;

  }
  
  
  formatLabel(value: number) {
    // console.log(value,'value');
    if (value) {
      // this.range = Math.round(value / 1000) + 'k';
      // console.log(this.range);
      // this.interstrange.from = '3%';
      // console.log(this.interstrange.from);
      this.to = value;
      console.log(this.to);

      // console.log('sisis');
      // // console.log(this.range);
      return (value  ) + ' %';
      
    }
    return value;
  }

  checkValue(event: any){
    console.log(event);
 }
 get(x) {
  
   this.id = x;
   console.log(this.id);
  // this.initial();
  }
  selectit(x) {
    console.log(x);
    console.log('ravi');
    console.log(this.bankname);
    if (!this.bankname[x]){
      this.bankname[x] = false;
    }
    this.bankname[x] = !this.bankname[x];
    console.log(this.bankname);

    //this.geting();
  }
  geting() {
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );

    let newdata = [];
    let i;
    console.log(this.bankname.length, 'mylength');
    for (i = 0; i < this.bankname.length; i++){
      console.log(this.bankname[i + 1]);
      if (this.bankname[i + 1] == true){
        console.log(this.name[i].id);
        newdata.push(this.name[i].id);
    }

    }


    console.log('sisis');
    console.log(this.bankname);
    console.log(newdata);
    let temp : any = {};
    temp.id = newdata;
 // temp.interest = {upper: this.formatLabel(this.newrange.upper), lower: this.formatLabel(this.newrange.lower)};
    temp.interest = this.newrange;
    let postData =  { id: newdata};
    
 //   let postData = '{ userid: "'+this.bankname+'"};';
    console.log(postData);
  
    this.global.http.post(this.global.urls + 'loanlisting.php',  temp, {observe: 'response'})
   
       .subscribe(data => {
        // console.log(data.body);
  
       this.banklist = data.body['data'];
       this.global.banks = this.banklist.data;
       this.filterstate = 0;
       console.log('filter');
       console.log(this.global.banks);
      //  this.list = '1';
       this.global.filter = '1';
  // this.loaded = '1';
       }, error => {

       });
  }


  rangefilter() {
    this.ranging = '1';
    this.interstrange.from = this.rangefrom + '%' ;
    this.interstrange.to = this.fil + '%' ;
    console.log(this.interstrange);
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );
  
    console.log(this.interstrange, 'range');
    let postData =  { interest: this.interstrange};
    console.log(postData, "postdata");
  
    this.global.http.post(this.global.urls + 'loanlisting.php', postData,  {observe: 'response'})
  
       .subscribe(data => {
        // console.log(data.body);
  
        this.banklist = data.body['data'];
        this.global.banks = this.banklist.data;
        console.log('filter');
        console.log(this.global.banks);
  
       }, error => {
  
       });
  }
  clearfilter(){
    this.global.filter = '0';
    this.ranging = '0';
    this.bankname = [];
    this.newrange = {upper: 40 , lower : 10};
    this.geting();


  }
  onInputChange(event: MatSliderChange) {
    console.log('This is emitted as the thumb slides');
    console.log(event.value);
    this.fil = event.value;
    console.log(this.fil);

    this.interstrange.from = this.rangefrom;
    this.interstrange.to = this.fil;
    console.log(this.interstrange);
  }

selectloan(x){

  this.flag = 1;
  this.loanid = x;

}

applyloan() {
  var headers = new Headers();
  headers.append('Accept', 'application/json');
  headers.append('Content-Type', 'application/json' );

  let temp ;
   
// temp.interest = {upper: this.formatLabel(this.newrange.upper), lower: this.formatLabel(this.newrange.lower)};
  this.formdata.userid = this.userid;
  // this.formdata.turnover = this.turnover;
  // this.formdata.pan = this.pan;
  let postData =  { data: this.formdata};
  
//   let postData = '{ userid: "'+this.bankname+'"};';
  // console.log(postData);

  this.global.http.post(this.global.urls + 'applyloan.php',  postData, {observe: 'response'})
 
     .subscribe(data => {
      // console.log(data.body);

     if(data.body['status']=='success'){
      this.loanapply = 'yes';
     }
  
// this.loaded = '1';
     }, error => {

     });
}

fileChange(event) {
  this.files = event.target.files;
  console.log(this.files);
}
applyit() {
 




  this.buttonloading = true;
  // this.formdata = this.formInput;
  console.log(this.formdata);
  // const token = localStorage.getItem('token');
  const headers = new Headers();
  // this.formInput.sid = this.selectedsite ;
  const postData = this.formInput;
  const httpOptions = {
    headers: new HttpHeaders({
      Accept: 'application/json',
      // Authorization: token
    })
  };
  let formData: FormData = new FormData();
  console.log(this.uploadedFiles);
  this.formdata.userid = this.userid;
  const file: File = this.formdata.file;
  console.log(this.files);

  formData.append('file', this.files[0], this.files[0].name);
  console.log('formData');
  console.log(formData);

  console.log(this.formInput);
 // console.log(this.formInput.toString());
  formData.append('data', JSON.stringify(this.formdata));
  formData.append('name', 'ss');


  console.log(formData);

  this.global.http.post(this.global.urls + 'applyloan.php', formData, httpOptions)
  // this.global.http.post(this.global.urls + 'applyloan.php',  postData, {observe: 'response'})

    .subscribe(data => {
      console.log(data);
      // tslint:disable-next-line: triple-equals
      if (data['status'] == 'success') {
        // this.modal.hide();
        // tslint:disable-next-line
      //  this.lenders('yes');
       // this.ngOnInit();
        this.uploadedFiles = [];
        this.loanapply='yes'
        // this.global.addToast({
        //   title: 'Lender added successfully',
        //   msg: 'Turning standard Success alerts into awesome notifications',
        //   timeout: 8000,
        //   theme: 'default',
        //   position: 'top-right',
        //   type: 'success'
        // })
       // this.loading = false;
        this.buttonloading = false;

      } else {
        //   alert();
        // tslint:disable-next-line
        this.buttonloading = false;
        
      }
    }, error => {
      console.log(error);
      //this.loading = false;

    });
}


}
