import { Component, OnInit } from '@angular/core';
import { Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { SignupComponent } from '../../components/signup/signup.component';
import { GlobalService } from '../../services/global.service';
import { PopupsService } from '../../services/popups.service';
import { TextAnimation } from 'ngx-teximate';
import * as aa from 'ng-animate';
import {  ViewChild, AfterViewInit } from '@angular/core';
@Component({
  selector: 'app-intro2',
  templateUrl: './intro2.component.html',
  styleUrls: ['./intro2.component.scss']
})
export class Intro2Component implements OnInit {
  clicked = '1';
  // tslint:disable-next-line: max-line-length
  // tslint:disable-next-line: quotemark
  backgroundimage = 'poultry';
  rocket = {
    path: 'https://assets9.lottiefiles.com/temp/lf20_KTRzeS.json',
    renderer: 'canvas',
    autoplay: true,
    loop: true
};
selected;
otp = 'no';
enterAnimation: TextAnimation = {
  animation: aa.fadeInDown,
  delay: 100,
  type: 'paragraph'
};
  change = true;
constructor( public popups: PopupsService , public global: GlobalService, public dialog: MatDialog) { }

  ngOnInit() {
  //  this.selectit('gg');
let ii = 0;
setInterval(() => {
  this.next();
  console.log('Test');
//this.change = !this.change;
        // ii = 0;
        // // tslint:disable-next-line: triple-equals
        // if (this.backgroundimage == 'poultry' && ii == 0) {
        //   this.backgroundimage = 'bakery';
        //   ii = 1;
        // }
        // if (this.backgroundimage == 'bakery' && ii == 0) {
        //   this.backgroundimage = 'hotel';
        //   ii = 1;

        // }
        // if (this.backgroundimage == 'hotel' && ii == 0) {
        //   this.backgroundimage = 'poultry';
        //   ii = 1;

        // }

    }, 8000);
}


next(){
  // tslint:disable-next-line: triple-equals
  if (this.backgroundimage == 'poultry' ) {
    this.backgroundimage = 'bakery';
    return;
  }
  if (this.backgroundimage == 'bakery') {
    this.backgroundimage = 'hotel';
    return;

  }
  if (this.backgroundimage == 'hotel') {
    this.backgroundimage = 'factory1';
    return;

  }
  if (this.backgroundimage == 'factory1') {
    this.backgroundimage = 'poultry';
    return;

  }
}
goback(){
  // tslint:disable-next-line: triple-equals
  if (this.backgroundimage == 'poultry' ) {
    this.backgroundimage = 'hotel';
    return;
  }
  if (this.backgroundimage == 'bakery') {
    this.backgroundimage = 'poultry';
    return;
  }
  if (this.backgroundimage == 'hotel') {
    this.backgroundimage = 'bakery';
    return;

  } 
}

  // openDialog(): void {
  //   // tslint:disable-next-line: no-use-before-declare
  //   const dialogRef = this.dialog.open(SignupComponent, {
  //     minWidth: '400px',
  //     height: '545px',
  //     hasBackdrop: true,
  //     data: {selected: 'signup'}

  //   });

  // }


  selectit(type) {
    // this.clicked='0';

    // this.otp='getotp';

    // return;
    this.global.storage.set('selected', type);
    this.selected = type;
    this.clicked = '0';
    console.log(this.selected);
  }
  back() {
    this.clicked = '1';
  }
}
