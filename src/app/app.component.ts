import { Component, OnInit, Renderer2 } from '@angular/core';
import * as AOS from 'aos';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'sme';
  constructor(private renderer: Renderer2) {}

  ngOnInit() {
    const loader = this.renderer.selectRootElement('#entryloader');
    loader.style.display = 'none';
    AOS.init();
  }
}
