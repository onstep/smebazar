import { Component, OnInit } from '@angular/core';
import {GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-chooseloan',
  templateUrl: './chooseloan.component.html',
  styleUrls: ['./chooseloan.component.scss']
})
export class ChooseloanComponent implements OnInit {

  constructor(public global: GlobalService) { }

  ngOnInit() {
  }

}
