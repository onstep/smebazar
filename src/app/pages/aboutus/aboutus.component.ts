import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { PopupsService } from '../../services/popups.service';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss']
})
export class AboutusComponent implements OnInit {
  manfly = {
    path: '/assets/manfly.json',
    renderer: 'canvas',
    autoplay: true,
    loop: true
};
  constructor(public global: GlobalService,public popups: PopupsService) { }

  ngOnInit() {
    this.global.storage.set('selected', 'technologies');

  }

}
