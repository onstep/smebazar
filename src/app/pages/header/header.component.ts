import { Component, OnInit, AfterViewChecked, HostListener  } from '@angular/core';
import * as $ from 'jquery';
import { ViewChild } from '@angular/core';
import { PopupsService } from '../../services/popups.service';
import { GlobalService } from '../../services/global.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @ViewChild('pageMenu')
  menuStickyElement;
  offsetTop: number;
  stickyNavMenu: boolean;
  whitecolor: any =' ';
  url;
  thishome;
  logo: any='1';
  constructor(public popups: PopupsService, public global: GlobalService) { }

  ngOnInit() {
    this.url = window.location.href; 
    //alert(this.url.Segments.Last());

    //this.url= this.url.Substring(this.url.LastIndexOf('/'));
    var parts = this.url.split('/');
    var answer = parts[parts.length - 1];
  
    if(answer=='' || answer=='home'){
//alert('yes its a home');
this.whitecolor='whitecolor';
this.thishome='yes';
 
    }else{
      this.whitecolor ='';
      this.thishome='no';

    }
  }
  @HostListener('window:scroll', [])
  sticklyMenu() {
    // Get top height 
    this.offsetTop = document.documentElement.scrollTop;
    // if (document.documentElement.scrollTop >= this.menuStickyElement.nativeElement.offsetTop) {
    //   console.log(document.documentElement.scrollTop);
    //   console.log(this.menuStickyElement.nativeElement.offsetTop);

    //   this.stickyNavMenu = true;
    //   console.log('white');
    // } else {
    //   console.log(this.menuStickyElement.nativeElement.offsetTop);

    //   this.stickyNavMenu = false;
    //   console.log('trasparent');
    // }
    if(document.documentElement.scrollTop==0){
      console.log('white');
      this.stickyNavMenu = false;
      if(this.thishome=='yes'){
        this.whitecolor='whitecolor';

      }
    }else{
      console.log('trans');
      this.stickyNavMenu = true;
    
      if(this.thishome=='yes'){
        this.whitecolor='';
      }
    }
  }
colr(){
  if(this.whitecolor=='whitecolor'){
    this.whitecolor='';

  }else{
      this.whitecolor='whitecolor';

  }
}

}
