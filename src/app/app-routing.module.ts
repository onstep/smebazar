import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { DrawComponent } from './pages/draw/draw.component';
import { LoanlistingComponent } from './pages/loanlisting/loanlisting.component';
import { LoancompareComponent } from './pages/loancompare/loancompare.component';
import { HeaderComponent } from './pages/header/header.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ComplianceComponent } from './pages/compliance/compliance.component';
import { FundingComponent } from './pages/funding/funding.component';
import { RatingComponent } from './pages/rating/rating.component';
import { FooterComponent } from './pages/footer/footer.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { MobileheaderComponent } from './components/mobileheader/mobileheader.component';
import {MobilehomeComponent } from './components/mobilehome/mobilehome.component';
import { TechnologyComponent } from './pages/technology/technology.component';
import { MobileonboardComponent } from './components/mobileonboard/mobileonboard.component';
import { SignupComponent } from './components/signup/signup.component';
import { AboutusComponent } from './pages/aboutus/aboutus.component';
import { Home10Component } from './pages/home10/home10.component';
import { Intro4Component } from './components/intro4/intro4.component';
import { SlidehomeComponent } from './components/slidehome/slidehome.component';
import {TermsComponent } from './pages/terms/terms.component';
import {PrivacyComponent } from './pages/privacy/privacy.component';
import {IfscComponent } from './pages/ifsc/ifsc.component';
import { CalculatorComponent } from './pages/calculator/calculator.component';
import { FilterComponent } from './pages/filter/filter.component';
import { ChooseloanComponent } from './pages/chooseloan/chooseloan.component';
import { ToolsComponent } from './pages/tools/tools.component';
import { MobilecalculatorComponent } from './pages/mobilecalculator/mobilecalculator.component';
const routes: Routes = [

  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'draw', component: DrawComponent },
  { path: 'loanlisting', component: LoanlistingComponent },
  { path: 'loancompare', component: LoancompareComponent },
  { path: 'calculator', component: CalculatorComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'compliance', component: ComplianceComponent },
  { path: 'funding', component: FundingComponent },
  { path: 'rating', component: RatingComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'mobileheader', component: MobileheaderComponent },
  { path: 'mobilehome', component: MobilehomeComponent },
  { path: 'technologies', component: TechnologyComponent },
  { path: 'mobileonboard', component: MobileonboardComponent },
  { path: 'about', component: AboutusComponent },
  { path: 'mobileonboard/:type', component: MobileonboardComponent },
  { path: 'signup', component: SignupComponent },
   { path: 'home10', component: Home10Component },
   { path: 'ifsc', component: IfscComponent },
  { path: 'intro4', component: Intro4Component },
  { path: 'slidehome', component: SlidehomeComponent },
  { path: 'termsandconditions', component: TermsComponent },
  { path: 'filter', component: FilterComponent },
  { path: 'privacypolicy', component: PrivacyComponent },
  { path: 'chooseloan', component: ChooseloanComponent },
  { path: 'mobilecalculator', component: MobilecalculatorComponent },
  { path: 'tools', component: ToolsComponent },
  { path: '**', component: NotfoundComponent }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
    })
  ],  exports: [RouterModule]
})
export class AppRoutingModule { }
