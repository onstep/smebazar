import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-mobileheader',
  templateUrl: './mobileheader.component.html',
  styleUrls: ['./mobileheader.component.scss']
})
export class MobileheaderComponent implements OnInit {
  menuStickyElement;
  offsetTop: number;
  stickyNavMenu: boolean;
  whitecolor: any =' ';
  url;
  thishome;
  constructor(public global: GlobalService) { }

  ngOnInit() {
    this.url = window.location.href; 
    //alert(this.url.Segments.Last());

    //this.url= this.url.Substring(this.url.LastIndexOf('/'));
    var parts = this.url.split('/');
    var answer = parts[parts.length - 1];
  
    if(answer=='' || answer=='home'){
//alert('yes its a home');
this.whitecolor='whitecolor';
this.thishome='yes';
 
    }else{
      this.whitecolor ='';
      this.thishome='no';

    }
  }
  openNav() {
    document.getElementById('myNav').style.width = '100%';
  }
  closeNav() {
    document.getElementById('myNav').style.width = '0%';
  }
}
