import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseloanComponent } from './chooseloan.component';

describe('ChooseloanComponent', () => {
  let component: ChooseloanComponent;
  let fixture: ComponentFixture<ChooseloanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseloanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseloanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
