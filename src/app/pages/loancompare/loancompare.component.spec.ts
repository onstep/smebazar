import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoancompareComponent } from './loancompare.component';

describe('LoancompareComponent', () => {
  let component: LoancompareComponent;
  let fixture: ComponentFixture<LoancompareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoancompareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoancompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
