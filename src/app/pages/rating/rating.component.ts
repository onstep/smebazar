import { Component, OnInit } from '@angular/core';
import {GlobalService } from '../../services/global.service';
import { PopupsService } from '../../services/popups.service';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

  constructor(public global: GlobalService,public popups: PopupsService ) { }

  ngOnInit() {
    this.global.storage.set('selected', 'rating');

  }

}
