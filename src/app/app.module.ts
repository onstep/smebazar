import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { TeximateModule } from 'ngx-teximate';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { DrawComponent } from './pages/draw/draw.component';
import * as $ from 'jquery'; 
import { LoanlistingComponent } from './pages/loanlisting/loanlisting.component';
import { HeaderComponent } from './pages/header/header.component';
import { IntroComponent } from './components/intro/intro.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {A11yModule} from '@angular/cdk/a11y';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTreeModule} from '@angular/material/tree';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ComplinceComponent } from './complince/complince.component';
import { ComplianceComponent } from './pages/compliance/compliance.component';
import { FundingComponent } from './pages/funding/funding.component';
import { RatingComponent } from './pages/rating/rating.component';
import { LottieAnimationViewModule } from 'ng-lottie';
import { TestdriveComponent } from './components/testdrive/testdrive.component';
import { TechnologyComponent } from './pages/technology/technology.component';
import { FooterComponent } from './pages/footer/footer.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { MobileheaderComponent } from './components/mobileheader/mobileheader.component';
import { MobilehomeComponent } from './components/mobilehome/mobilehome.component';
import { MobileonboardComponent } from './components/mobileonboard/mobileonboard.component';
import { Sform } from './components/mobileonboard/mobileonboard.component';
import { IonicModule } from '@ionic/angular';
import { SignupComponent } from './components/signup/signup.component';
import { getotp } from './components/signup/signup.component';
import { DesktopsignupComponent } from './components/desktopsignup/desktopsignup.component';
import { FormsModule, FormGroup } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Intro2Component } from './components/intro2/intro2.component';
import { LoandetailsComponent } from './components/loandetails/loandetails.component';
import { IonicStorageModule } from '@ionic/storage';
import { NotifierModule } from 'angular-notifier';
import { AboutusComponent } from './pages/aboutus/aboutus.component';
import { Home10Component } from './pages/home10/home10.component';
import { Intro5Component } from './components/intro5/intro5.component';
import { Intro6Component } from './components/intro6/intro6.component';



import { Intro3Component } from './components/intro3/intro3.component';
import { Home5Component } from './pages/home5/home5.component';
import { Intro4Component } from './components/intro4/intro4.component';
import { SwiperModule, SwiperConfigInterface, SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SlidehomeComponent } from './components/slidehome/slidehome.component';
import { HeaderforslideComponent } from './components/headerforslide/headerforslide.component';
import { ContentforhomeComponent } from './components/contentforhome/contentforhome.component';
import { TermsComponent } from './pages/terms/terms.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';
import { LoancompareComponent } from './pages/loancompare/loancompare.component';
import { CalculatorComponent } from './pages/calculator/calculator.component';
import { FilterComponent } from './pages/filter/filter.component';
import { IfscComponent } from './pages/ifsc/ifsc.component';
import { TrackingComponent } from './pages/tracking/tracking.component';
// import { FlexLayoutModule } from '@angular/flex-layout';


import {  ReactiveFormsModule } from '@angular/forms';
import { MobileloanlistingComponent } from './pages/mobileloanlisting/mobileloanlisting.component';
import { ChooseloanComponent } from './pages/chooseloan/chooseloan.component';
import { MobilecalculatorComponent } from './pages/mobilecalculator/mobilecalculator.component';
import { ToolsComponent } from './pages/tools/tools.component';
@NgModule({  
  declarations: [
    AppComponent,
    HomeComponent,
    DrawComponent,
    LoanlistingComponent,
    HeaderComponent,
    IntroComponent, 
    DashboardComponent,
    ComplinceComponent,
    ComplianceComponent,
    FundingComponent,
    RatingComponent,
    TestdriveComponent,
    TechnologyComponent,
    FooterComponent,
    NotfoundComponent,
    MobileheaderComponent,
    MobilehomeComponent,
    MobileonboardComponent,
    Sform,
    SignupComponent,
    getotp,
    DesktopsignupComponent,
    Intro2Component,
    LoandetailsComponent,
    AboutusComponent,
    Home10Component,
    Intro5Component,
    Intro6Component,
    Intro3Component,
    Home5Component,
    Intro4Component,
    SlidehomeComponent,
    HeaderforslideComponent,
    ContentforhomeComponent,
    TermsComponent,
    PrivacyComponent,
    LoancompareComponent,
    CalculatorComponent,
    FilterComponent,
    IfscComponent,
    TrackingComponent,
    MobileloanlistingComponent,
    ChooseloanComponent,
    MobilecalculatorComponent,
    ToolsComponent
  ],
  entryComponents: [Sform,getotp],

  imports: [
    ReactiveFormsModule,
    NotifierModule.withConfig({position: {

      horizontal: {
        position: 'middle',
        distance: 12
      },
      vertical: {
        position: 'top',
        distance: 122,
        gap: 10
      }
    }}),
    FormsModule,
    TeximateModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    PortalModule,
    ScrollingModule,
    LottieAnimationViewModule.forRoot(),
    DeviceDetectorModule.forRoot(),
    IonicModule.forRoot({mode:'ios'}),
    HttpClientModule,
    IonicStorageModule.forRoot(),
    SwiperModule,
    // FlexLayoutModule




    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

