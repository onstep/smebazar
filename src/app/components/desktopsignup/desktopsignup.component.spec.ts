import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesktopsignupComponent } from './desktopsignup.component';

describe('DesktopsignupComponent', () => {
  let component: DesktopsignupComponent;
  let fixture: ComponentFixture<DesktopsignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesktopsignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesktopsignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
