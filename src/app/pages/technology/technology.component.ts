import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { PopupsService } from '../../services/popups.service';

@Component({
  selector: 'app-technology',
  templateUrl: './technology.component.html',
  styleUrls: ['./technology.component.scss']
})
export class TechnologyComponent implements OnInit {

  constructor(public global: GlobalService,public popups: PopupsService) { }

  ngOnInit() {
    this.global.storage.set('selected', 'technologies');

  }

}
