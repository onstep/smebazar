import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentforhomeComponent } from './contentforhome.component';

describe('ContentforhomeComponent', () => {
  let component: ContentforhomeComponent;
  let fixture: ComponentFixture<ContentforhomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentforhomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentforhomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
