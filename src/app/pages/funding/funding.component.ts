import { Component, OnInit } from '@angular/core';
import {GlobalService } from '../../services/global.service';
import { PopupsService } from '../../services/popups.service';

@Component({
  selector: 'app-funding',
  templateUrl: './funding.component.html',
  styleUrls: ['./funding.component.scss']
})
export class FundingComponent implements OnInit {
flag:any=0;
  constructor(public global: GlobalService,public popups: PopupsService) { }

  ngOnInit() {
    this.global.storage.set('selected', 'funding');

  }
  open(value){
    this.flag=value;

  }

}
