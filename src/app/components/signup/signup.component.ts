import { Component, OnInit, Optional } from '@angular/core';
import { Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Location } from '@angular/common';
import { GlobalService } from '../../services/global.service';
import { PopupsService } from '../../services/popups.service';
import {Router} from '@angular/router';

export interface DialogData {
  mobile: string;
  name: string;
  selected: any;
}
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
selected: any = 'signup';
name: string;
mobile: any;
city= '';
otp: any;
otpv: any;
userid: any;
password: any;
  loading: string;
formdata: any = {};
  // tslint:disable-next-line: max-line-length
  constructor(private router: Router, @Optional() @Inject(MAT_DIALOG_DATA) public datax: any,public dialogRef: MatDialogRef<getotp>, public global: GlobalService, public location: Location, public dialog: MatDialog) { }

  ngOnInit() {
    console.log(this.datax);
 //this.global.toast(this.datax.selected);
    if (this.datax.selected) {
  this.selected = this.datax.selected;
 }
  }
  onNoClick(): void {
    //this.global.toast();
     this.dialogRef.close();
     
   }
  invalid() {
   this.global.toast('Invalid Userid and Password');
  }
  cancel() {
    this.location.back();
  }



  login() {
if (!this.name) {
  this.global.toast('Please enter name');
  return;
}
if (!this.city) {
  this.global.toast('Please select city');
  return;
}
if (!this.mobile) {

  //this.global.toast(this.name);

  this.global.toast('Please enter valid mobile number');
  return;
}
if (String(this.mobile.value).length != 9) {

  console.log(String(this.mobile.value).length);
  this.global.toast('Please enter valid mobile number');
  return;
}

this.loading = 'yes';
let headers = new Headers();
headers.append('Accept', 'application/json');
headers.append('Content-Type', 'application/json' );

const postData =  { userid: this.mobile};

this.global.http.post(this.global.url+'/otp.php', postData, {observe: 'response'})
.subscribe(data => {
         this.loading = 'no';

         console.log(data.body);
         const ddata = data.body;
         // tslint:disable-next-line: triple-equals
         if (ddata['status'] == 'sucess') {
 //this.global.toast('insert otp ');
 this.otpv = ddata['otp'];
 this.openDialog();


         } else {
           this.loading = 'no';

         }
        }, error => {
         this.loading = 'no';

         console.log(error);
       });
   }

  openDialog(): void {
    if (this.global.ismobile) {
   // tslint:disable-next-line: no-use-before-declare
    const dialogRef = this.dialog.open(getotp, {
      minWidth: '90%',
      hasBackdrop: false,
      data: {name: this.name, mobile: this.mobile, city: this.city, otpv: this.otpv}
    });
    this.onNoClick();
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.otp = result;
    });
    } else {
      // tslint:disable-next-line: no-use-before-declare
      const dialogRef = this.dialog.open(getotp, {
      minWidth: '400px',
      hasBackdrop: false,
      data: {name: this.name, mobile: this.mobile, city: this.city, otpv: this.otpv}
    });
      this.onNoClick();

      dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
     // this.cancel();
      this.otp = result;
    });
    }



  }


}
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'getotp',
  templateUrl: 'getotp.html',
})
// tslint:disable-next-line: component-class-suffix
export class getotp {
otp1: any;
loanmenu: any = 'no';
status: any = '';
formdata: any = {};

  loading: string;
  success = {
    path: '/assets/success.json',
    renderer: 'canvas',
    autoplay: true,
    loop: true
};
  constructor(
    private router: Router,
    public global: GlobalService,
    public dialogRef: MatDialogRef<getotp>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.formdata.type = '';
    }

    verifyit() {
     console.log(this.otp1);
     console.log(this.data.otpv);
     console.log(this.data);
     if (this.otp1 == this.data.otpv) {
this.proceed();
// this.router.navigate(['/dashboard']); 
      } else {
        //alert('Invalid otp');
        this.global.toast("Invalid OTP");
       
      }
    }
  onNoClick(): void {
   //this.global.toast();
  //  this.dialogRef.afterClosed().subscribe((val) => {
  //   this.router.navigate(['/']); 

  //  });
   this.dialogRef.close();

  }

  updateloan(){
    if(!this.formdata.companyname || this.formdata.companyname== '' ){
this.global.toast('Please Enter valid Company Name');
return;
    } 
    if(!this.formdata.companynature || this.formdata.companynature== '' ){
      this.global.toast('Please Enter valid Company Nature');
      return;
    } 
    if(!this.formdata.amount || this.formdata.amount== '' ){
      this.global.toast('Please Enter valid loan amount');
      return;
    }
      // tslint:disable-next-line: align
      if(!this.formdata.type || this.formdata.type== '' ){
        this.global.toast('Please Enter valid loan type');
        return;
    } 
    this.formdata.mobile=this.data.mobile;
  
    this.updatenow();
  }
  proceed() {


    this.loading = 'yes';
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );

    const postData =  { name: this.data.name, mobile: this.data.mobile, city: this.data.city};

    this.global.http.post(this.global.url + '/getdata.php', postData, {observe: 'response'})
           .subscribe(data => {
             this.loading = 'no';

             console.log(data.body);
             const ddata = data.body;
             // tslint:disable-next-line: triple-equals
             if (ddata['status'] == 'success') {
              this.global.storage.get('selected').then((val) => {
              if (val=='funding'){
             //this.global.toast('funding');
              this.loanmenu='yes';
              }
              });
              this.status = 'success';
     //this.global.toast('insert otp ');
     //this.global.toast('You have beed succesfully registered.Our team will contact you soon');


             } else {
               this.loading = 'no';

             }
            }, error => {
             this.loading = 'no';

             console.log(error);
           });
       }
       updatenow() {


        this.loading = 'yes';
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json' );
    
        const postData =  { data: this.formdata};
    
        this.global.http.post(this.global.url + 'loans.php', postData, {observe: 'response'})
               .subscribe(data => {
                 this.loading = 'no';
    
                 console.log(data.body);
                 const ddata = data.body;
                 // tslint:disable-next-line: triple-equals
                 if (ddata['status'] == 'success') {
                  this.dialogRef.close();
                  this.router.navigate(['/chooseloan']);
                  this.global.storage.set('userid',ddata['projectid']);
                  this.global.storage.get('selected').then((val) => {
                  if (val=='funding'){
                 //this.global.toast('funding');
                  this.loanmenu='no';
                  
                  }
                  });
                  this.status = 'success';
         //this.global.toast('insert otp ');
         //this.global.toast('You have beed succesfully registered.Our team will contact you soon');
    
    
                 } else {
                   this.loading = 'no';
    
                 }
                }, error => {
                 this.loading = 'no';
    
                 console.log(error);
               });
           }
}
