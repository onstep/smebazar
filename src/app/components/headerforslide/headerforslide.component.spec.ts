import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderforslideComponent } from './headerforslide.component';

describe('HeaderforslideComponent', () => {
  let component: HeaderforslideComponent;
  let fixture: ComponentFixture<HeaderforslideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderforslideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderforslideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
