import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobilecalculatorComponent } from './mobilecalculator.component';

describe('MobilecalculatorComponent', () => {
  let component: MobilecalculatorComponent;
  let fixture: ComponentFixture<MobilecalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobilecalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobilecalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
