import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-mobilecalculator',
  templateUrl: './mobilecalculator.component.html',
  styleUrls: ['./mobilecalculator.component.scss']
})
export class MobilecalculatorComponent implements OnInit {

  constructor(public global: GlobalService) { }

  ngOnInit() {
  }

}
