import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { PopupsService } from '../../services/popups.service';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit {

  constructor(public global: GlobalService,public popups: PopupsService) { }

  ngOnInit() {
    this.global.storage.set('selected', 'terms');

  }

}
