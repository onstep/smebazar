import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlidehomeComponent } from './slidehome.component';

describe('SlidehomeComponent', () => {
  let component: SlidehomeComponent;
  let fixture: ComponentFixture<SlidehomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlidehomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlidehomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
