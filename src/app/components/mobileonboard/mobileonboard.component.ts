import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {ActivatedRoute} from '@angular/router';
import { PopupsService } from '../../services/popups.service';

@Component({
  selector: 'app-mobileonboard',
  templateUrl: './mobileonboard.component.html',
  styleUrls: ['./mobileonboard.component.scss']
})
export class MobileonboardComponent implements OnInit {
type: any = 'smeloans';
  constructor(public popups: PopupsService,private activeRoute: ActivatedRoute,private location: Location,private _bottomSheet: MatBottomSheet) {
    this.type = this.activeRoute.snapshot.paramMap.get('type');
   }

  ngOnInit() {
  }
  cancel() {
    this.location.back();
  }
  openBottomSheet(): void {
    // tslint:disable-next-line: no-use-before-declare
    this._bottomSheet.open(Sform);
  }
  
}

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'Sform',
  templateUrl: 'sform.html',
})
// tslint:disable-next-line: component-class-suffix
export class Sform {
  // tslint:disable-next-line: variable-name
  constructor(private _bottomSheetRef: MatBottomSheetRef<Sform>) {}

  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }
}


