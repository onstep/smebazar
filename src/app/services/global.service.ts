import { Injectable } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import Swal from 'sweetalert2';
import { NotifierService } from 'angular-notifier';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
ismobile: any = false;
loaded: any = false;
filter: any = '0';
banks:any=[];
// url = 'https://goshift.onstep.in/';
url = 'http://13.126.161.129/smebackend/';
urls = 'http://13.126.161.129/smebackend/';

  // tslint:disable-next-line: max-line-length
  constructor(private router: Router, public notifierService: NotifierService, public storage: Storage, public http: HttpClient, public dialog: MatDialog, public deviceService: DeviceDetectorService) {

    this.ismobile = this.deviceService.isMobile();
    this.loaded = true;
   }
 toast2(msg) {
  Swal.fire(
    msg,
    '',
    'error',

  );
 }
 toast(msg) {
  this.notifierService.notify('error', msg);

 }
 scrolltowhy() {
  // tslint:disable-next-line: prefer-const
  this.router.navigate(['/']);
  setTimeout(() => {
        console.log('Test');
        const elmnt = document.getElementById('why');
        elmnt.scrollIntoView();
    }, 100);

} scrolltoabout() {
  // tslint:disable-next-line: prefer-const
  this.router.navigate(['/']);
  setTimeout(() => {
        console.log('Test');
        const elmnt = document.getElementById('about');
        elmnt.scrollIntoView();
    }, 100);

}
}
