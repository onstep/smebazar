import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-ifsc',
  templateUrl: './ifsc.component.html',
  styleUrls: ['./ifsc.component.scss']
})
export class IfscComponent implements OnInit {
  banks: any;
  states: any;
  districts: any;
  branchs: any;
  loading: any = false;
  formdata: any = {};
  ifscdata : any = {};
  ifscloaded:any = false;
  constructor(public global: GlobalService) { }

  ngOnInit() {
    this.initial('all');
  }
  axis() {
    alert('IFSC CODE IS -- UTIB0000082') ;
  }
  initial(type) {
    this.loading = true;
    console.log('sme');

    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json' );

    if (type == 'all') {
      this.formdata.bank = '';
      this.formdata.state = '';
      this.formdata.district = '';
      this.formdata.branch = '' ;
  }
   // tslint:disable-next-line: align
   if (type == 'bank') {
     this.formdata.state = '';
     this.formdata.district = '';
     this.formdata.branch = '' ;
   }
    if (type == 'state') {
     this.formdata.district = '';
     this.formdata.branch = '' ;
   }
   // tslint:disable-next-line: align
   if (type == 'district') {
     this.formdata.branch = '' ;
   }
    if (type == 'branch') {
     this.formdata.ifsc = '' ;
   }
    const postData =  this.formdata;

    this.global.http.post(this.global.urls + 'ifsc.php', postData,  {observe: 'response'})

       .subscribe(data => {
        // console.log(data.body);

       if (type == 'all') {
        this.banks = data.body['data'];

      }
       if (type == 'bank') {
        this.states = data.body['data'];
      }
      // tslint:disable-next-line: align
      if (type == 'state') {
        this.districts = data.body['data'];
      }
       if (type == 'district') {
        this.branchs = data.body['data'];

      }
       if (type == 'branch') {
        this.ifscdata = data.body['data'][0];
        this.ifscloaded = true;

      }
       if (type != 'branch') {
        this.ifscloaded = false;

      }
 

       console.log(this.banks);
       this.loading = false;

       }, error => {

       });
  }
}
