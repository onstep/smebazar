import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { PopupsService } from '../../services/popups.service';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.scss']
})
export class PrivacyComponent implements OnInit {

  constructor(public global: GlobalService,public popups: PopupsService) { }

  ngOnInit() {
    this.global.storage.set('selected', 'terms');
  }

}
