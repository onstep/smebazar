import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileloanlistingComponent } from './mobileloanlisting.component';

describe('MobileloanlistingComponent', () => {
  let component: MobileloanlistingComponent;
  let fixture: ComponentFixture<MobileloanlistingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileloanlistingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileloanlistingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
