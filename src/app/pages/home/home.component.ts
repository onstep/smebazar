import { Component, OnInit, AfterViewChecked, HostListener  } from '@angular/core';
import * as $ from 'jquery';
import { ViewChild } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { from } from 'rxjs';

// import {
//     TweenMax,
//     TimelineLite,
//     TimelineMax,
//     Linear
// } from 'gsap';
// import ScrollMagic from 'scrollmagic';
// import * as ScrollMagic from 'scrollmagic'; // Or use scrollmagic-with-ssr to avoid server rendering problems
// import { TweenMax, TimelineMax } from "gsap"; // Also works with TweenLite and TimelineLite
// import { ScrollMagicPluginGsap } from 'scrollmagic-plugin-gsap';
// import 'imports-loader?define=>false!scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js';

// import 'imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
// ScrollMagicPluginGsap(ScrollMagic, TweenMax, TimelineMax);

// import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
// import 'gsap';
// import * as DrawSVGPlugin from 'gsap';// i can comment that out, same behaviour -.-
// import 'imports-loader?define=>false!animation.gsap'; // needed due to bug in ScrollMagic
// import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
@HostListener('window:scroll', ['$event'])
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // ctrl = new ScrollMagic.Controller();
  svg;
  manfly = {
    path: '/assets/flyman.json',
    renderer: 'canvas',
    autoplay: true,
    loop: true
};
  constructor(public global: GlobalService) { }

  ngOnInit() {

  }

}
